package com.saluti.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DualListModel;

import com.saluti.bean.model.CidadeModel;
import com.saluti.service.CidadeService;

@ManagedBean(name="pickListView")
@ViewScoped
public class PickListView implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@ManagedProperty("#{cidadeService}")
	private CidadeService cidadeService;
	
    private DualListModel<CidadeModel> cidadesVisitadasPick;

	public DualListModel<CidadeModel> getCidadesVisitadasPick() {
		return cidadesVisitadasPick;
	}

	public void setCidadesVisitadasPick(DualListModel<CidadeModel> cidadesVisitadasPick) {
		this.cidadesVisitadasPick = cidadesVisitadasPick;
	}
	
	@PostConstruct
	public void init() {
		List<CidadeModel> cidadesNaoVisitadas = cidadeService.getCidadesNaoVisitadas();
		List<CidadeModel> cidadesVisitadas = cidadeService.getCidadesVisitadas();
		
		if(cidadesVisitadasPick == null) {
			cidadesVisitadasPick = new DualListModel<CidadeModel>(cidadesNaoVisitadas, cidadesVisitadas);
		}
	}

	public CidadeService getCidadeService() {
		return cidadeService;
	}

	public void setCidadeService(CidadeService cidadeService) {
		this.cidadeService = cidadeService;
	}
	
	public void salvar() {
		System.out.println(this.cidadesVisitadasPick);
	}
	

}
