package com.saluti.bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.saluti.dao.TestDAO;
import com.saluti.model.User;

@ManagedBean(name = "usuarioBean")
@SessionScoped
public class UsuarioBean {
    public List<User> getListaUsuarios(){
    	TestDAO dao = new TestDAO();
    	return dao.recuperaUsuarios();
    };
}
