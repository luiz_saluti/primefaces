package com.saluti.bean.model;

import java.io.Serializable;

import com.saluti.bean.AbstractBean;

public class CidadeModel extends AbstractBean{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nome;
	private String pais;
	private int codigo;

	public CidadeModel(String cidade, String pais) {
		this.nome = cidade;
		this.pais = pais;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
    
}  
