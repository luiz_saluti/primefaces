package com.saluti.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.saluti.model.User;

public class TestDAO {
	SessionFactory factory;
	
   public List<User> recuperaUsuarios(){   
	   factory = new Configuration().configure().buildSessionFactory();
	   Session session = factory.openSession();
	   return session.createNamedQuery("User.listar", User.class).getResultList();
   }
}
