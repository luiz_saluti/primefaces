package com.saluti.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.saluti.bean.model.CidadeModel;

@ManagedBean
@SessionScoped
public class CidadeService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static int codigo = 1;

	public List<CidadeModel> getCidadesNaoVisitadas() {
		List<CidadeModel> cidades = new ArrayList<>();
		cidades.add(geraCidade("Paris", "Franca"));
		cidades.add(geraCidade("Caxias", "Brazil"));
		cidades.add(geraCidade("Nova Iorque", "EUA"));
		return cidades;
	}
	
	public List<CidadeModel> getCidadesVisitadas() {
		List<CidadeModel> cidades = new ArrayList<>();
		cidades.add(geraCidade("Belo Horizonte", "Brasil"));
		cidades.add(geraCidade("Londres", "Inglaterra"));
		cidades.add(geraCidade("Ontario", "Canada"));
		return cidades;
	}
	
	public CidadeModel geraCidade(String cidade, String pais) {
		CidadeModel cidadeModel = new CidadeModel(cidade, pais);
		cidadeModel.setId(codigo);
		cidadeModel.setCodigo(codigo++);
		return cidadeModel;
	}
	

}
